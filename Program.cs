﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MagicNumbers
{
    public static class Program
    {
        public static void Main()
        {
            var token = WriteToken(DateTime.UtcNow, "123456789");
            Console.WriteLine(token);

            Console.WriteLine(IsValidToken(token));
        }

        public static string WriteToken(DateTime timestamp, string randomValue)
        {
            if (randomValue.Length != 9 || randomValue.Any(c => !char.IsDigit(c)))
                throw new ArgumentException("Random value must be exacly 9 characters long and consist only of digits", nameof(randomValue));

            // year(4 digits) month(2 digits) day(2 digits) hour (24-hour clock, 2 digits) minute(2 digits) second(2 digits)
            var timeStampString = timestamp.ToUniversalTime().ToString("yyyyMMddHHmmss");

            const string magicNum = "61";

            var stringBuilder = new StringBuilder(28);
            stringBuilder.Append(timeStampString);
            stringBuilder.Append(magicNum);
            stringBuilder.Append(randomValue);

            var payload = stringBuilder.ToString();

            var checksum = GetChecksum(payload);
            stringBuilder.Append(checksum);

            var res = stringBuilder.ToString();

            Debug.Assert(res.Length == 28, "Token length is not 28");

            return res;
        }

        public static string GetChecksum(string payload)
        {
            if (payload.Length != 25 || payload.Any(c => !char.IsDigit(c)))
                throw new ArgumentException("Payload must be exactly 25 characters long and consist only of digits", nameof(payload));

            byte checksum = 0;

            for (int i = 0; i < payload.Length; i++)
            {
                char c = payload[i];
                var b = Convert.ToByte(c);

                byte d = (byte)(i % 14);
                unchecked
                {
                    checksum += (byte)(b * d);
                }
            }

            // Exactly 3 digits
            return checksum.ToString("000");
        }

        public static bool IsValidToken(string token)
        {
            // Must be 28 characters long and consist only of digits
            if (token.Length != 28)
                return false;

            var payload = token.Substring(0, 25);

            // Verify checksum
            var tokenChecksum = token.Substring(25, 3);
            var actualChecksum = GetChecksum(payload);
            if (tokenChecksum != actualChecksum)
                return false;

            // Verify that date is parseable
            var dateStr = token.Substring(0, 14);
            if (!DateTime.TryParseExact(dateStr, "yyyyMMddHHmmss", null, DateTimeStyles.None, out var _))
                return false;

            return true;
        }
    }
}
